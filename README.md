# Google Map API Libary

## How to use:
RUN IN TERMINAL 
```
npm i
gulp
```

Create div with id "map", and add options with prefix `map-`

Available options:

`map-center="[latitude], [longtitude]";`

`map-zoom="[int_number]";`

`map-style="[json_file_name_with_styles]"`, premade styles:
* aubergine,
* dark,
* night,
* retro,
* silver,
* standard

`map-disableTypeIds="[TypeIdName],[TypeIdName],..."`;

`map-markers="[json_file_name_with_markers]"`, premade markers:
* example

`map-markerAnimation="[true/false]"`;

`map-startAnimation="[true/false]"` - if set to true, markers are dropped at the load;

`map-onClickMarker="[true/false]"` - if set to true, adds marker to the point clicked on the map;

`map-geolocation="[true/false]"` - if set to true, asks for geolocation and centers map to host position;

There are also custom functions like:
```
OnClickMarker("[MarkerTitle]","[MarkerLabel]","[MarkerIcon]","[MarkerCanBeDragged - true/false]", "[Animation - true/false]");
searchMarker("[keyword]", [id]);
```