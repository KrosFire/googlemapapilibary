let gMap;
let markersTAB = [];

function initMap() {
  $(document).ready(() => {
    // Define all JSON files
    let jsonFiles = [];

    // Map creation
    const createMap = () => {
      gMap = new google.maps.Map(
        document.getElementById('map'), {
          zoom: zoom,
          center: center,
          styles: mapStyle,
          mapTypeControlOptions: {
            mapTypeIds: TypeIds
          }
        }
      )

      //Append Markers
      appendMark();

      //Add on Event Markers
      if (map.attr("map-onClickMarker") != undefined && map.attr("map-onClickMarker").trim().toLowerCase() == "true") {
        addMarker();
      }

      //Get Geolocation
      if (map.attr("map-geolocation") != undefined && map.attr("map-geolocation").trim().toLowerCase() == "true") {
        getLocation();
      }
    }

    // Check if lat and long are defined
    const map = $("#map");
    let center;

    if (map.attr("map-center") != undefined && map.attr("map-center").trim() != "") {
      const latLong = map.attr("map-center").trim().split(",");
      center = {
        lat: parseFloat(latLong[0]),
        lng: parseFloat(latLong[1])
      }
    } else {
      center = {
        lat: 52.237049,
        lng: 21.017532
      };
    }

    // Check if zoom is defined
    let zoom;

    if (map.attr("map-zoom") != undefined && map.attr("map-zoom").trim() != "") {
      zoom = parseInt(map.attr("map-zoom").trim());
    } else {
      zoom = 8;
    }

    //Check if Type Ids are disabled
    let TypeIds = ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map'];

    const mapDisable = map.attr("map-disableTypeIds");

    if (mapDisable != undefined && mapDisable.trim() != "") {
      const disabledTab = mapDisable.trim().toLowerCase().split(",");

      for (let d of disabledTab) {
        TypeIds.splice($.inArray(d, TypeIds), 1);

      }
    }

    //Check if Markers are defined
    const marekersDefined = map.attr("map-markers").trim().split(",");
    let markersActive = false;

    if (map.attr("map-markers") != undefined && map.attr("map-markers").trim() != "") {
      //Iteraiting on all markers files
      for (let m of marekersDefined) {
        let markersPath = `markers/${m}.json`;
        //Pushing json files
        jsonFiles.push(
          $.getJSON(markersPath, jsonFile => {
            //Geting data from json file
            jsonFile.forEach(data => {
              //Creating Markers
              const position = new google.maps.LatLng(data.position[0], data.position[1]);
              const title = data.title;
              const label = data.label;
              const drag = data.draggable;
              const id = data.id;
              const desc = data.description;
              const marker = new google.maps.Marker({
                position: position,
                id: id,
                title: title,
                label: label,
                draggable: drag,
                description: desc
              })

              // Setting drop Animation
              if (map.attr("map-startAnimation") != undefined && map.attr("map-startAnimation").trim().toLowerCase() == "true") {
                marker.setAnimation(google.maps.Animation.DROP);
              }

              // Setting Bounce Animation
              if (map.attr("map-markerAnimation") != undefined && map.attr("map-markerAnimation").trim().toLowerCase() == "true") {
                marker.addListener("click", toggleBounce);
                marker.getAnimation(null)
              }

              // Bounce Animation
              function toggleBounce() {
                if (marker.getAnimation() != 1) {
                  marker.setAnimation(google.maps.Animation.BOUNCE);
                } else {
                  marker.setAnimation(null);
                }
              }

              //Pushing Markers to TAB
              markersTAB.push(marker);

            })
          })
        );
      }
      //Setting Markers to Active
      markersActive = true;
    }
    // Append Markers function
    const appendMark = () => {
      if (markersActive) {
        for (let m of markersTAB) {
          m.setMap(gMap);
        }
      }
    }

    // On click Marker creation STANDARD
    const addMarker = () => {
      google.maps.event.addListener(gMap, "click", e => {
        const marker = new google.maps.Marker({
          position: e.latLng,
          map: gMap
        })
        createdMarkers.push(marker);
      })
    }

    // Check if Geolocation is wanted
    
      const getLocation = () => {
        navigator.geolocation.getCurrentPosition(pos => {
          var position = {
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
          }
          gMap.setCenter(position);
        })

      }
    


    // Check if styles are defined
    let mapStyle;
    if (map.attr("map-style") != undefined && map.attr("map-style").trim() != "") {
      const styleName = map.attr("map-style").trim().toLowerCase();
      const stylePath = `mapStyles/${styleName}.json`;
      //Pushing json files
      jsonFiles.push(
        $.getJSON(stylePath, e => {
          mapStyle = e;
        })
      );
    }

    //Watching first 4 json files to be loaded
    let all = $.when(jsonFiles[0], jsonFiles[1], jsonFiles[2], jsonFiles[3]);
    all.done(() => {
      //Creating map
      createMap();
    })
  });

}
// On click Marker creation CUSTOM
let createdMarkers = [];
const OnClickMarker = (title, label, icon, drag, animation) => {
  //Check if map is loaded
  if (gMap != undefined) {
    //Clear other onClick functions
    if (google.maps.event.hasListeners(gMap, "click")) {
      google.maps.event.clearListeners(gMap, "click");
    }
    //Create onClick Listener
    google.maps.event.addListener(gMap, "click", e => {
      //Create marker
      const marker = new google.maps.Marker({
        position: e.latLng,
        map: gMap,
        title: title,
        label: label,
        icon: icon,
        draggable: drag
      });
      //Setting Animation
      if (animation === true) {
        marker.addListener("click", function () {
          if (marker.getAnimation() != 1) {
            marker.setAnimation(google.maps.Animation.BOUNCE);
          } else {
            marker.setAnimation(null);
          }
        })
      }
      createdMarkers.push(marker);
    });
  } else {
    setTimeout(OnClickMarker, 100, title, label, icon, drag, animation);
  }
}

//Remove added Markers to the map
const removeMarkers = (type, perm) => {
  // PreDefined Loops
  const removeStatic = () => {
    for (let m of markersTAB) {
      m.setMap(null);
    }
  }
  const removeDynamic = () => {
    for (let c of createdMarkers) {
      c.setMap(null);
    }
  }
  // Waiting for a map to load
  if (gMap != undefined) {
    if (type != undefined && type.trim() != "") {
      // Remove only static
      if (type.trim().toLowerCase() == "static") {
        removeStatic();
      }
      // Remove only dynamic
      else if (type.trim().toLowerCase() == "dynamic") {
        removeDynamic();
      }
      // Remove all markers
      else if (type.trim().toLowerCase() == "all") {
        removeDynamic();
        removeStatic();
      }
      //Remove them permamently
      if (perm === true && type.trim().toLowerCase() == "dynamic") {
        createdMarkers = [];
      } else if (perm === true && type.trim().toLowerCase() == "static") {
        markersTAB = [];
      } else if (perm === true && type.trim().toLowerCase() == "all") {
        createdMarkers = [];
        markersTAB = [];
      }
    }
  } else {
    setTimeout(removeMarkers, 100, type, perm);
  }

}

//Show Markers
const showMarkers = (type, id) => {
  // Predefine loops
  const showStatic = () => {
    for (let m of markersTAB) {
      m.setMap(gMap);
    }
  };
  const showDynamic = () => {
    for (let c of createdMarkers) {
      c.setMap(gMap);
    }
  }
  if (gMap != undefined) {
    if (type != undefined && type.trim() != "") {
      if (type.trim().toLowerCase() == "static") {
        showStatic();
      } else if (type.trim().toLowerCase() == "dynamic") {
        showDynamic();
      } else if (type.trim().toLowerCase() == "all") {
        showStatic();
        showDynamic();
      }
    }

    //Show markers with matching ID
    if (id != undefined && id != "") {
      for (let m of markersTAB) {
        m.setMap(null);
        if (m.id == id) {
          m.setMap(gMap);
        }
      }
    }
  } else {
    setTimeout(showStaticMarkers, 100, type, id)
  }
}

// Create static Marker
const createMarker = (pos,title, label, desc, icon, drag, animation) =>{
  //Checking if map is loaded
  if(gMap != undefined){
    pos = pos.trim().split(",");
    const position = new google.maps.LatLng(pos[0], pos[1]);
    const marker = new google.maps.Marker({
      position: position,
      title:title,
      map:gMap,
      label:label,
      description:desc,
      icon:icon,
      draggable:drag
    })
    // Checking if animation is defined
    if(animation != undefined && animation == true){
      marker.addListener("click", function () {
        if (marker.getAnimation() != 1) {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        } else {
          marker.setAnimation(null);
        }
      })
    }
    markersTAB.push(marker);
    marker.setMap(gMap);
  }else{
    setTimeout(createMarker, 100, pos, title, label, desc, icon, drag, animation)
  }
}

//Search for Marker
const searchMarker = (keyword, id) =>{
  //Checking if map is loaded
  if(gMap != undefined){
    // Remove all Markers
    removeMarkers("all");
    let searchResult = [];
    //Check if keyword is defined
    if(keyword != undefined && keyword.trim() != ""){
      // Making sure everything matches
      keyword = keyword.toLowerCase();
      // Looking on every marker
      for(let m of markersTAB){
        // Checking if marker has description
        if(m.description != undefined){
          // Making sure everything matches
          const desc = m.description.toLowerCase();
          // Looking if descripton includes keyword
          if(desc.includes(keyword)){
            // Checking if id is defined
            if(id != undefined && id != ""){
              // Showing Markers that have matching id
              if(m.id == id){
                m.setMap(gMap);
                searchResult.push(m);
              }
            }else{
              m.setMap(gMap);
              searchResult.push(m);
            }
          }
        }
      }
    }else if(id != undefined && id != ""){
      for(let m of markersTAB){
        if(m.id == id){
          m.setMap(gMap);
          searchResult.push(m);
        }
      }
    }else{
      return console.error("ERROR: At least one parameter needed!")
    }
    console.log(searchResult);
    return searchResult;
  }else{
    setTimeout(searchMarker, 100, keyword, id);
  }
}